FROM public.ecr.aws/lambda/nodejs:18
RUN yum install -y git && yum clean all
RUN npm i -g yarn && npm cache clean --force
CMD ["node"]
